package DAO;

import java.sql.Connection;
import java.util.List;
public abstract class DAO<T> {
	//Permet d'établir la connexion à la BDD
		public Connection connect=ConnexionPostgreSql.getInstance();
		
		public abstract void create (T obj);
		
		public abstract T read (String id);
		
		public abstract void update (T obj);
		
		public abstract void delete (T obj);
		
		public abstract List<T> recupAll();
}
