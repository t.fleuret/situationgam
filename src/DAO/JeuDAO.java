package DAO;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classe.Jeu;
import classe.Joueur;


public class JeuDAO extends DAO<Jeu> {

	
	public List<Jeu> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Jeu> listeJeu = new ArrayList<Jeu>(); 

		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();
			Statement requete2 = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"appli_jeu\".jeux");
			 
			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Club
				Jeu unJeu = new Jeu();
				Joueur unJoueur = new Joueur();
				
				unJeu.setId(curseur.getInt("id"));
				unJeu.setnom(curseur.getString("nom"));
				unJeu.setDescription(curseur.getString("description"));
				unJeu.setDate_creation(curseur.getString("date"));
				unJeu.setPrix(curseur.getInt("prix"));
				unJeu.setImage(curseur.getString("image"));
				unJeu.setConfig_recommandee(curseur.getString("config_recomandée"));
				
				
				// faire req jointure entre jouer et joueur + condition id_jeu == curseur.getInt("id")
				ResultSet curseur2 = requete2.executeQuery("select j.* from \"appli_jeu\".joueurs j "
				  		+ "inner join \"appli_jeu\".jouer on jouer.id_joueur = j.id "
				  		+ "where id_jeu = "+curseur.getInt("id"));

				List<Joueur> listeJoueur = new ArrayList<Joueur>(); 
				while (curseur2.next()){ 
					
					//	 ==>>> ajouter un élément Joueur dans la List de Joueurs de l'objet Jeu unJeu)
					listeJoueur.add(new Joueur(curseur2.getInt("id"),curseur2.getString("pseudo"),curseur2.getString("nationalite"),curseur2.getInt("age")));
					//listeJoueur.add(unJoueur);
				}
				unJeu.setJoueurs_jeu(listeJoueur);
				
				listeJeu.add(unJeu);			
		    	curseur2.close(); 
			}

			
			curseur.close();
			requete.close();
			requete2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeJeu; 
	}

	@Override
	public void create(Jeu obj) {
		try {
		 	PreparedStatement prepare = this.connect
                                        .prepareStatement("INSERT INTO \"appli_jeu\".jeux VALUES(?, ?, ?, ?,?,?,?)"
                                         );
		 	prepare.setInt(1, obj.getId());
			prepare.setString(2, obj.getNom());
			prepare.setString(3, obj.getDescription());
			prepare.setString(4, obj.getDate_creation());
			prepare.setInt(3, obj.getPrix());
			prepare.setString(3, obj.getImage());
			prepare.setString(3, obj.getConfig_recommandee());
				
			prepare.executeUpdate();  
				
		}
	catch (SQLException e) {
	e.printStackTrace();
	} 
		
	}

		
		
	// Recherche d'un club par rapport à son code	
	@Override
	public Jeu read(String id) {
	
		Jeu leJeu = new Jeu();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"appli_jeu\".jeux WHERE id = '" + id +"'");
	          
	          if(result.first())
	           		leJeu = new Jeu(result.getInt("id"), result.getString("nom"),result.getString("description"),result.getString("date_creation"),result.getInt("prix"),result.getString("image"),result.getString("config_recomande"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leJeu;
		
	}
		
	
	// Mise à jour d'un Club
	@Override
	public void update(Jeu obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"appli_jeu\".jeux SET id = '" + obj.getId() + "'"+
	                    	      " WHERE code = '" + obj.getId()+"'" );

		}
		catch (SQLException e) {
		      e.printStackTrace();
		}

	}


	// Suppression d'un Club
	@Override
	public void delete(Jeu obj) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"appli_jeu\".jeux WHERE code = '" + obj.getId()+"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}



	
	
}

