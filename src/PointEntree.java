
import controleur.ControleurPrincipal;
import vues.Vue_principale;

public class PointEntree {

		public static void main(String args[]) {
			ControleurPrincipal leControleur = new ControleurPrincipal() ;
			Vue_principale laVue = new Vue_principale (leControleur);
			leControleur.setVues(laVue);
			laVue.setVisible(true);

			}
	}


