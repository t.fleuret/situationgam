package vues;
import java.awt.BorderLayout;
	import java.awt.EventQueue;
	import java.util.ArrayList;
	import java.util.List;

	import javax.swing.JFrame;
	import javax.swing.JPanel;
	import javax.swing.JTextArea;
	import javax.swing.border.EmptyBorder;

import DAO.ConnexionPostgreSql;
import classe.Jeu;
import classe.Joueur;
import controleur.ControleurPrincipal;

import javax.swing.ImageIcon;
	import javax.swing.JComboBox;
	import javax.swing.JTextField;
	import javax.swing.JList;
	import javax.swing.JLabel;
	import java.awt.event.ActionListener;
	import java.awt.event.ActionEvent;
	import java.awt.Color;
	import java.awt.SystemColor;

	public class Vue_principale extends JFrame {

	    private JPanel contentPane;
	    private static List<Jeu> les_jeux = new ArrayList<Jeu>();
	    private JTextField textField;
	    private ControleurPrincipal controleur;
	    private JTextArea desc;
	    private JTextArea joueur;
	    private JLabel prix ;
	    private JLabel date_creation;
	    private JLabel conf;
	    private JLabel image;
	    public JComboBox affiche_jeu;
	  
		public Vue_principale(ControleurPrincipal controleur) {
			this.controleur=controleur;
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 557, 484);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			 affiche_jeu = new JComboBox();
			affiche_jeu.setActionCommand("comboBox");
			affiche_jeu.addActionListener(controleur);
		/* 	affiche_jeu.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				
					desc.setText("");
					joueur.setText("");
					prix.setText("");
					date_creation.setText("");
					conf.setText("");
					
					int jeux_Choisi = affiche_jeu.getSelectedIndex();
				//	desc.setText(les_jeux.get(jeux_Choisi).getnom());
					
					desc.setText(les_jeux.get(jeux_Choisi).getdescription());
					prix.setText(""+les_jeux.get(jeux_Choisi).getprix());
					date_creation.setText(les_jeux.get(jeux_Choisi).getdate_creation());
					conf.setText(les_jeux.get(jeux_Choisi).getconfig_recommandee());
					date_match.setText(les_jeux.get(jeux_Choisi).genere_date_prochain_match());
				
					image.setIcon(new ImageIcon(Vue_principale.class.getResource("/gaming/"+les_jeux.get(jeux_Choisi).getimage()+"")));
					
				for (Joueur jo : les_jeux.get(jeux_Choisi).getJoueurs_jeu()) {
					joueur.setText(joueur.getText()+jo.getpseudo()+"\n");
				}	
				}
			}); */
			
			affiche_jeu.setBounds(109, 11, 198, 22);
			contentPane.add(affiche_jeu);
			
			
			//zone de texte
			JLabel lblNewLabel = new JLabel("joueur :");
			affiche_jeu.addActionListener(controleur);

			lblNewLabel.setBounds(12, 249, 98, 23);
			contentPane.add(lblNewLabel);
			
			JLabel lblNewLabel_1 = new JLabel("configuration recommandee : ");
			lblNewLabel_1.setBounds(12, 353, 233, 28);
			contentPane.add(lblNewLabel_1);
			
			JLabel lblConfigurationPrix = new JLabel("prix:");
			lblConfigurationPrix.setBounds(12, 209, 46, 28);
			contentPane.add(lblConfigurationPrix);
			
			JLabel lblDateCreation = new JLabel("date creation :");
			lblDateCreation.setBounds(12, 393, 119, 28);
			contentPane.add(lblDateCreation);
			
			JLabel lblDescription = new JLabel("description :");
			lblDescription.setBounds(13, 54, 95, 28);
			contentPane.add(lblDescription);
			
			// zone o� apparaitront les informations concernant le jeu
			desc = new JTextArea("");
			desc.setLineWrap(true);
			desc.setBounds(137, 45, 231, 158);
			contentPane.add(desc);
			
		    prix  =new JLabel("");
			prix.setBounds(126, 203, 119, 28);
			contentPane.add(prix);
			
		    date_creation = new JLabel("");
		    date_creation.setBounds(143,396, 198,25 );
			contentPane.add(date_creation);
			
		    conf =new JLabel("");
			conf.setBounds(237, 353, 205, 28);
			contentPane.add(conf);
			
			joueur = new JTextArea("");
			joueur.setBackground(SystemColor.control);
			joueur.setBounds(121, 249, 276, 59);
			contentPane.add(joueur);
			
			image = new JLabel("");
			//0
			image.setBounds(378, 25, 155, 184);
			contentPane.add(image);
			
/*
				for (Jeu j : les_jeux) {
					affiche_jeu.addItem(j.getnom()); //comboBox donnant les noms des jeux
					
				}
				*/
				}
		public void remplirComboJeu(List<Jeu> lesJeu) {
			affiche_jeu.addItem("-");
			for (Jeu c : lesJeu) {			
				affiche_jeu.addItem(c.getNom());
			}
		}
		public void Quitter() {
			ConnexionPostgreSql.Arreter(); 
			System.exit(0);
			
		}

		public void affichageInfoJeu(Jeu j) {
			desc.setText(j.getDescription());
			prix.setText(j.getPrix()+"");//on met le int entre crochet car on ne peut pas mettre de int dans un texte
			date_creation.setText(j.getDate_creation());
			conf.setText(j.getConfig_recommandee());
			joueur.setText("");
			for (Joueur leJoueur : j.getJoueurs_jeu()) {
				joueur.setText(joueur.getText()+leJoueur.getPseudo()+"\n");
			}
			image.setIcon(new ImageIcon(Vue_principale.class.getResource("../img/"+j.getImage())));
			// affiche_jeu.setText(c.getNom()+" -Le nom du jeu "+c.getdescription()+" description "+c.getdate_creation()+" date ");
		}
	}

